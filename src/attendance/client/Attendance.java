package attendance.client;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Attendance {
    private Long id;
    @NotNull
    private Timestamp Attendancetime;
    @NotNull
    @Size(min = 5, max = 16)
    private String staff_username;
    private int year;
    @NotNull
    private int month;
    @NotNull
    private int day;
    @NotNull
    private int state;//2是请假，1是已打卡，0是缺勤


    public Attendance(Long id, Timestamp attendancetime, String staff_username, int state) {
        super();
        this.id = id;
        this.Attendancetime = attendancetime;
        this.staff_username = staff_username;
        this.state = state;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("attendancetime" + attendancetime);
        String str = sdf.format(attendancetime);
        this.year = Integer.parseInt(str.split("-")[0]);
        this.month = Integer.parseInt(str.split("-")[1]);
        this.day = Integer.parseInt(str.split("-")[2]);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getAttendancetime() {
        return Attendancetime;
    }

    public void setAttendancetime(Timestamp attendancetime) {
        Attendancetime = attendancetime;
    }

    public String getStaff_username() {
        return staff_username;
    }

    public void setStaff_username(String staff_username) {
        this.staff_username = staff_username;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
	
	
