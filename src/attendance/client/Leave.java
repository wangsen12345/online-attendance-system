package attendance.client;

import java.util.Date;

public class Leave {
    private Long id;
    private String staff_username;
    private Date leavetime;
    private int Duration;
    private int state;//0是正在审核，1是已经审核通过，2是销假，3是已经过期
    private String manager_username;
}
