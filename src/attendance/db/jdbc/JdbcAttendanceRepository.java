package attendance.db.jdbc;

import attendance.db.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcAttendanceRepository implements AttendanceRepository {
    private final JdbcTemplate jdbc;

    @Autowired
    public JdbcAttendanceRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
}
