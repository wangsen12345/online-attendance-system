package attendance.db.jdbc;

import attendance.client.Attendance;
import attendance.client.Staff;
import attendance.db.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import attendance.web.Modifystaff;
import attendance.web.PaginationSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class JdbcStaffRepository implements StaffRepository {
    private final JdbcTemplate jdbc;

    @Autowired
    public JdbcStaffRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Staff findByUserName(String userName, String password) {
        // TODO 自动生成的方法存根
        Staff staff = null;
        try {
            staff = jdbc.queryForObject("select * from staff where username=? and password=?", new StaffRowMapper(),
                    userName, password);


        } catch (DataAccessException e) {
        }
        return staff;

    }

    @Override
    public boolean addAttendance(String username) {
        // TODO 自动生成的方法存
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String str = sdf.format(date);
        int year, month, day;
        year = Integer.parseInt(str.split("-")[0]);
        month = Integer.parseInt(str.split("-")[1]);
        day = Integer.parseInt(str.split("-")[2]);
        Attendance attendance = null;
        try {
            attendance = jdbc.queryForObject("select * from attendance where staff_username=? and year=? and month=? and day=? and state=1", new AttendanceRowMapper(),
                    username, year, month, day);


        } catch (DataAccessException e) {
        }
        if (attendance == null) {
            jdbc.update("insert into attendance (Attendancetime, staff_username,year, month, day,state) values (?, ?, ?, ?, ?,?)", date, username, year, month, day, 1);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void modify(Modifystaff staffinfo, Long id) {
        // TODO 自动生成的方法存根
        jdbc.update("update staff set name=? where id=?", staffinfo.getName(), id);
        jdbc.update("update staff set email=? where id=?", staffinfo.getEmail(), id);
        jdbc.update("update staff set phoneNo=? where id=?", staffinfo.getPhoneNo(), id);
        jdbc.update("update staff set address=? where id=?", staffinfo.getAddress(), id);
    }

    @Override
    public Staff findByUserName(Long id) {
        Staff staff = null;
        try {
            staff = jdbc.queryForObject("select * from staff where id=?", new StaffRowMapper(),
                    id);
        } catch (DataAccessException e) {
        }
        return staff;
    }

    @Override
    public PaginationSupport<Attendance> attendancePage(int pageNo, int pageSize, String username) {
        int totalCount = jdbc.queryForInt("select count(id) from attendance where staff_username=?", username);
        int startIndex = PaginationSupport.convertFromPageToStartIndex(pageNo, pageSize);
        if (totalCount < 1)
            return new PaginationSupport<Attendance>(new ArrayList<Attendance>(0), 0);

        List<Attendance> items = jdbc.query("select * from attendance where staff_username=? order by id desc limit ? offset  ?", new AttendanceRowMapper(), username, pageSize, startIndex);
        System.out.println(items.get(0).getAttendancetime());
        PaginationSupport<Attendance> ps = new PaginationSupport<Attendance>(items, totalCount, pageSize, startIndex);
        return ps;
    }

    private static class StaffRowMapper implements RowMapper<Staff> {
        public Staff mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Staff(rs.getLong("id"), rs.getString("username"), null, rs.getString("Name"),
                    rs.getString("email"), rs.getString("phoneNo"), rs.getInt("department_id"), rs.getString("address"));
        }
    }

    private static class AttendanceRowMapper implements RowMapper<Attendance> {
        public Attendance mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Attendance(rs.getLong("id"), rs.getTimestamp("Attendancetime"), rs.getString("staff_username"), rs.getInt("state"));
        }
    }

}
