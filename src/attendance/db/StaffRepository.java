package attendance.db;

import attendance.client.Attendance;
import attendance.client.Staff;
import attendance.web.Modifystaff;
import attendance.web.PaginationSupport;

public interface StaffRepository {

    Staff findByUserName(String userName, String password);

    /**
     * 添加员工的打卡信息
     *
     * @param uername 员工用户名
     * @return 是否添加成功（一天只能打一次卡）
     */
    boolean addAttendance(String uername);

    void modify(Modifystaff staffinfo, Long id);

    Staff findByUserName(Long id);

    PaginationSupport<Attendance> attendancePage(int pageNo, int pageSize, String username);


}
