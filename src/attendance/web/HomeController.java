package attendance.web;

import attendance.client.Staff;
import attendance.db.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.swing.*;
import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


/**
 * 系统主页控制类
 *
 * @author wben
 * @version v1.0
 */
@Controller // 控制定义
@RequestMapping("/") // 相应web路径
public class HomeController {

    @Autowired // 自动注入资源
    private StaffRepository staffRepository;

    @RequestMapping(method = POST) // 处理首页打卡操作
    public String doAttendance(HttpSession session) {
        Staff staff = (Staff) session.getAttribute("staff");
        String id = staff.getUsername();
        boolean flag = staffRepository.addAttendance(id);
        if (flag) {
            JOptionPane.showMessageDialog(null, "打卡成功");
        } else
            JOptionPane.showMessageDialog(null, "不能重复打卡");
        return "redirect:/";
    }

    @RequestMapping(method = GET) // 相应的请求方法
    public String home(Model model) {
        return "home";
    }

    /**
     * 注册
     *
     * @return
     */
    @RequestMapping(value = "/login", method = GET)
    public String showLoginForm() {
        return "loginForm";
    }


    /**
     * 登录请求
     *
     * @param userName
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "/login", method = POST)
    public String processLogin(@RequestParam(value = "userName", defaultValue = "") String userName,
                               @RequestParam(value = "password", defaultValue = "") String password, HttpSession session) {

        Staff staff = staffRepository.findByUserName(userName, password);
        if (staff != null) {
            session.setAttribute("staff", staff);
            return "redirect:/";
        } else {
            return "loginError";
        }

    }

    /**
     * 注销
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "/logout", method = GET)
    public String logout(HttpSession session) {
        session.removeAttribute("staff");
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/modifyinformation", method = GET)
    public String modify(Model model, HttpSession session) {
        Staff staff = (Staff) session.getAttribute("staff");
        model.addAttribute("modifystaff", new Modifystaff(staff.getName(), staff.getEmail(), staff.getPhoneNo(), staff.getAddress()));
        return "modify";
    }

    @RequestMapping(value = "/modifyinformation", method = POST)
    public String processmodify(@Valid Modifystaff staffinfo, Errors errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "modify";
        }
        Staff staff = (Staff) session.getAttribute("staff");
        staffRepository.modify(staffinfo, staff.getId());
        Staff staff1 = staffRepository.findByUserName(staff.getId());
        System.out.println(staff1);
        session.setAttribute("staff", staff1);
        return "redirect:/";
    }

    @RequestMapping(value = "/attendancerecord", method = GET)
    public String checkedspittles(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                  @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, Model model, HttpSession session) {
        if (pageSize <= 0) {
            pageSize = PaginationSupport.PAGESIZE;
        }
        Staff staff = (Staff) session.getAttribute("staff");
        model.addAttribute("attendancepagSupport", staffRepository.attendancePage(pageNo, pageSize, staff.getUsername()));
        return "attendance";
    }

    @RequestMapping(value = "/leave", method = GET)
    public String leaveForm() {
        return "leave";
    }

}
