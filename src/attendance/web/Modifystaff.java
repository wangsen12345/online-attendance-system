package attendance.web;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Modifystaff {
    @NotNull
    @Size(min = 5, max = 16)
    private String name;
    @Email
    @NotNull
    private String email;
    @NotNull
    @Size(min = 1, max = 16)
    private String phoneNo;
    @NotNull
    @Size(min = 5, max = 16)
    private String address;


    public Modifystaff(String name, String email, String phoneNo, String address) {
        super();
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
        this.address = address;
    }

    public Modifystaff() {
        // TODO 自动生成的构造函数存根
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
