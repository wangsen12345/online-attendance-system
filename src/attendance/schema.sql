# drop schema if exists test;
# create schema test default character set utf8 collate utf8_general_ci;
# use test;
#
# drop table if exists Department;

create table Department
(
    ID       int,
    name     varchar(20) not null,
    parent_ID int references Department (ID),
    primary key (ID),
    foreign key (parent_ID) references Department (ID)
);

create table Staff
(
    ID            int auto_increment,
    username      varchar(20) unique not null,
    password      varchar(20)        not null,
    name          varchar(30)        not null,
    email         varchar(30)        not null,
    phoneNo       varchar(13)        not null,
    department_id int                not null,
    address       varchar(255)       not null,
    primary key (ID),
    foreign key (department_id) references Department (ID)
);

create table Attendance
(
    ID             integer auto_increment,
    attendanceTime datetime,
    staff_username varchar(30) not null,
    year           int         not null,
    month          int         not null,
    day            int         not null,
    state          int         not null,
    primary key (ID),
    foreign key (staff_username) references Staff (username)
);


insert into Department
values (1, 'A', null);
insert into Department
values (2, 'B', 1);
insert into Department
values (3, 'C', null);